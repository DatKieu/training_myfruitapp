package com.example.projectfruit.application

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@Suppress("EmptyClassBlock")
@HiltAndroidApp
class MyApplication : Application() {
}
