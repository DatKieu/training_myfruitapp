package com.example.projectfruit.common

class Constant {

    enum class KeyEvent {
        UPDATE_FRUIT,
        DELETE_FRUIT,
    }

    companion object{
        const val REQUEST_CAMERA_PERMISSION =1001
    }
}
